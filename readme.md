This repo stores my custom layout and other good to know or things to remember about this keyboard or qmk.

# Qmk links
- [Qmk Configurator](https://config.qmk.fm/#/redox/rev1/LAYOUT)
- [Qmk Test](https://config.qmk.fm/#/test)
- [Qmk Toolbox](https://github.com/qmk/qmk_toolbox/releases)
- [Qmk Keycodes](https://beta.docs.qmk.fm/using-qmk/simple-keycodes/keycodes_basic)

# Redox links
- [Redox GitHub](https://github.com/mattdibi/redox-keyboard)
- [LenBok Case GitHub](https://github.com/Lenbok/scad-keyboard-cases/tree/master/redox-rev0b)
- [My mod to LenBok's Case GitHub Fork](https://github.com/philsson/scad-keyboard-cases/tree/wider-usb-and-audio-plug) \
  Note that my changes are in a branch ```wider-usb-and-audio-plug```.

# Flashing
Flashing is done by powering the keyboard while holding the reset button down. Alternatively reseting the keyboard by a keypress, in my case ```Pg-Up + Q``` (Pg-Up is the rightmost key on the left side).
Just the left side of the keyboard does not need re-flashing when a layout is changed.
Use ```Qmk Toolbox``` to flash, its the easiest way to do it.

## Right side flashing
To flash this with a keyboard press, it is just the mirrored keybinding when this side is connected to the PC.

If not setting DFU mode through the keyboard there may be something fishi going on with this controller. To get it in DFU mode (flashing mode) I've had to "double reset" it. This is done by first powering it (shows up as a HID unit), then resetting it (I've used a jumper) twice (like a double click).
The right side is flashed with the same file as the left side. It is automatically recognized as right hand side when plugged in. For this the USB connector to the computer goes on the left side.

# Layout
Layer 0
![](figures/layer-0.png)
Layer 1
![](figures/layer-1.png)
Layer 2
![](figures/layer-2.png)
Layer 3
![](figures/layer-3.png)